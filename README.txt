INTRODUCTION
------------

This module generates charts of user points evolution and current status.
Three different blocks are provided that display different types of charts.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/userpoints_charts

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/userpoints_charts


REQUIREMENTS
------------

This module requires the following modules:

 * Userpoints: https://www.drupal.org/project/userpoints
 * Charts: https://www.drupal.org/project/charts

Userpoints must have at least one category configured. The default
"uncategorized" category does not count for this requirement, and is ignored
by this module.


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/895232/ for further information.


CONFIGURATION
-------------

 * Permissions:

   On the permissions page, two permissions are available:

   - Access all Userpoints Charts: Allows users to view anyone's charts
   - Access own Userpoints Charts: Only allows users to view their own charts

   Granting "Access all Userpoints Charts" to a role implicitly gives
   "Access own Userpoints Charts" to that role as well.

 * Charts:

   You can set some charts options at admin/config/content/charts . In
   particular, you can choose colors for chart elements. Some configuration
   options such as dimensions are currently not respected by this module.

   To set custom colors for Userpoints Charts only, go to the
   "Userpoints settings" page (admin/config/people/userpoints/settings). You
   will see a section called "Charts configuration". Uncollapsing this section
   will show an option to set custom colors for each userpoints category.


USAGE
-----

This module provides three blocks that display different types of charts:

    1. Userpoints line chart: A day-by-day history of a user's points for the
       past 7 days.

    2. Userpoints weekly growth bar chart: A chart showing a user's percentage
       gain in each category over the last week. The chart will not be
       displayed if no gains have been made in the last week or if the user is
       less than a week old.

    3. Userpoints pie chart: Shows the relative amount of a user's points
       between each category.
 
Enabling a block on a user page (user/%uid/*) will generate a chart for that
user. Enabling blocks on other pages will generate charts for the currently
logged in user's points.


TODO
----

 * Per-block configuration (chart size, category selection, etc...)
 * Handle expired transactions
 * Support other chart providers besides Google


CREDITS
-------

 * Development of this module is sponsored by youtropolis.com

 * This module is partially based on the Userpoints History module by ilo:
   https://www.drupal.org/project/userpoints_history
