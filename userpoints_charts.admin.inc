<?php

/**
 * @file
 * Administration pages or settings for the Userpoints Charts module.
 */

/**
 * Implements hook_form().
 */
function userpoints_charts_settings_form($form_state = NULL) {
  $form = array();
  $form['userpoints_charts_data_colors'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Data Colors'),
    '#description'    => t('Specify colors for each category using rrggbb notation, separated by commas. Example: 123456,789012,abcdef . Leave empty to use defaults.'),
    '#default_value'  => variable_get('userpoints_charts_data_colors', ''),
  );
  return $form;
}
